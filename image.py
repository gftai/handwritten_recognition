import PIL.Image
import PIL.ImageOps
import numpy as np
import torch
import sys, os
from PIL import ImageOps, ImageFilter, ImageEnhance

IMG_SIZE = 64, 48
TMP_IMG_DIR = "data/tmp_dump/"


def dopoint(p):
    if p == 0:
        return 0
    else:
        return 65336


class Image:

    def __init__(self, filepath: str):

        if not os.path.exists(TMP_IMG_DIR):
            os.makedirs(TMP_IMG_DIR)

        split = filepath.split("/")[-1].split("-")
        self.clazz = int(split[0][3:]) - 1

        self.filename = filepath.split("/")[-1]
        self.data = PIL.Image.open(filepath)
        self.data.thumbnail(IMG_SIZE)
        # self.data.thumbnail(IMG_SIZE, PIL.Image.ANTIALIAS)
        self.data = PIL.ImageOps.invert(self.data)
        self.data = self.data.point(dopoint)
        self.data = self.data.filter(ImageFilter.FIND_EDGES)
        # self.data = self.data.point(lambda p: 65336 if p > 0 else 0)
        # enh = ImageEnhance.Brightness(self.data)
        # self.data = enh.enhance(1000)
        self.data = [torch.from_numpy(np.asarray(im).astype(np.float32) - 128).view(-1) for im in
                     self.augment(self.data, self.filename)]

    @staticmethod
    def augment(data, filename):
        crop = 4
        data = data.convert('L')
        data_rot_A = data.rotate(-15, resample=PIL.Image.BILINEAR)
        data_rot_B = data.rotate(15, resample=PIL.Image.BILINEAR)
        images = [
            data,
            data_rot_A,
            data_rot_B,
            ImageOps.expand(data, crop, fill=1).resize(size=IMG_SIZE, resample=PIL.Image.BILINEAR),
            ImageOps.expand(data_rot_A, crop, fill=1).resize(size=IMG_SIZE, resample=PIL.Image.BILINEAR),
            ImageOps.expand(data_rot_B, crop, fill=1).resize(size=IMG_SIZE, resample=PIL.Image.BILINEAR),
            ImageOps.crop(data, border=crop).resize(size=IMG_SIZE, resample=PIL.Image.BILINEAR),
            ImageOps.crop(data_rot_A, border=crop).resize(size=IMG_SIZE, resample=PIL.Image.BILINEAR),
            ImageOps.crop(data_rot_B, border=crop).resize(size=IMG_SIZE, resample=PIL.Image.BILINEAR)
        ]

        for i, im in enumerate(images):
            im.save(TMP_IMG_DIR + filename + "__" + str(i) + ".png", "PNG")
        return images
