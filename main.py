import functools
import itertools
import multiprocessing
import os
import pickle
import string
from multiprocessing.pool import Pool

import torch
import torch.nn.functional as F
import torch.utils.data
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from torch import nn
from torch.nn import Module

from image import IMG_SIZE, Image

MAPPING = string.digits + string.ascii_uppercase + string.ascii_lowercase
LEARNING_RATE = 1e-3

class Net(nn.Module):
    def __init__(self, conv_start_size: int, kernel_size: int = 3):
        super(Net, self).__init__()

        self.activation = torch.tanh

        self.conv1 = nn.Conv2d(1, conv_start_size, kernel_size=kernel_size)
        self.conv2 = nn.Conv2d(conv_start_size, conv_start_size, kernel_size=kernel_size)
        self.conv3 = nn.Conv2d(conv_start_size, conv_start_size * 2, kernel_size=kernel_size)
        self.conv4 = nn.Conv2d(conv_start_size * 2, conv_start_size * 2, kernel_size=kernel_size)
        self.conv5 = nn.Conv2d(conv_start_size * 2, conv_start_size * 4, kernel_size=kernel_size)
        self.conv6 = nn.Conv2d(conv_start_size * 4, conv_start_size * 4, kernel_size=kernel_size)
        self.fc1 = nn.Linear(32 * conv_start_size, 32)
        self.fc2 = nn.Linear(32, 62)

    def convolve(self, convA, convB, x):
        x = self.activation(convA(x))
        x = self.activation(convB(x))
        return F.max_pool2d(x, 2)

    def forward(self, x):
        x = self.convolve(self.conv1, self.conv2, x)
        x = self.convolve(self.conv3, self.conv4, x)
        x = self.convolve(self.conv5, self.conv6, x)

        dim = functools.reduce(lambda a, b: a * b, x.size()[1:])

        x = x.view(-1, dim)
        x = self.activation(self.fc1(x))
        x = F.dropout(x, p=0.3, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


def process_single_dir(path): return ["{}/{}".format(path, f) for f in os.listdir(path)]


def load_or_cached(data_dir: str, debug: bool = False):
    try:
        with open('data/images.pickle', 'rb') as f:
            print("Loading cached data...")
            images = pickle.load(f)
            print("Done.")
            return images
    except (FileNotFoundError, IOError, EOFError):

        image_files = []

        for file in os.listdir(data_dir):
            if os.path.isdir(data_dir + file):
                image_files.extend(process_single_dir(data_dir + file))

        images = []

        if debug:
            print("Loading data (debug)...")
            images = [Image(f) for f in image_files]
        else:
            print("Loading data...")
            with Pool(processes=multiprocessing.cpu_count()) as pool:
                images = pool.map(Image, image_files)

        print("Pickling...")
        with open('data/images.pickle', 'wb') as f:
            pickle.dump(images, f)

        print("Done.")
        return images


def accuracy(net: Module, x_test, y_test, filenames=None):
    as_list = [net.eval()(x.view(-1, *x.size())).data.tolist()[0] for x in x_test]
    actual = [el.index(max(el)) for el in as_list]

    if filenames is not None:
        l0 = [a == b for a, b in zip(y_test, actual)]
        l1 = [a[1] for a in itertools.filterfalse(lambda a: a[0], zip(l0, filenames))]
        l2 = ["{}/{}".format(MAPPING[a], MAPPING[b]) for a, b in zip(y_test, actual)]
        l3 = [a[1] for a in itertools.filterfalse(lambda a: a[0], zip(l0, l2))]
        print("incorrect ({}): ".format(len(l1)) + ", ".join(
            ["{}({})".format(a, b) for a, b in zip(l1, l3)]))
    return accuracy_score(y_test, actual)


def main(seed: int, conv_start_size: int, data_dir: str, cuda: bool, kernel_size: int, debug: bool = False):
    """

    :param seed: Random seed. Used to make pseudo-random predictable.
    :param conv_start_size: Number of convolutional filters. Same for each convolution.
    :param data_dir: Location of input images.
    :param cuda: Whether to use CUDA (if available). CPU otherwise.
    :param kernel_size: Convolutional kernel size
    :param debug: If True, does not spawn multiple processes for image preprocessing (easier to debug)
    :return: None
    """
    if not torch.cuda.is_available():
        cuda = False
        print("CUDA is not available. Falling back to CPU.")

    device = torch.device("cuda" if cuda else "cpu")

    torch.manual_seed(seed)
    if cuda: torch.cuda.manual_seed(seed)

    images = load_or_cached(data_dir, debug)
    print("Images loaded: {}".format(len(images)))

    x = [i.view(1, IMG_SIZE[0], IMG_SIZE[1]) for flavors in images for i in flavors.data]
    y = [i.clazz for i in images for _ in range(len(i.data))]
    n = [i.filename for i in images for _ in range(len(i.data))]
    print("Dataset size: {}".format(len(x)))
    assert len(x) == len(y)

    x_train, x_test, y_train, y_test, n_train, n_test = train_test_split(x, y, n, test_size=0.2, random_state=42,
                                                                         stratify=y)
    x_train = torch.stack(x_train)
    x_test = torch.stack(x_test).to(device)

    dataset = torch.utils.data.TensorDataset(x_train, torch.LongTensor(y_train))
    trainloader = torch.utils.data.DataLoader(dataset, batch_size=32)

    net = Net(conv_start_size, kernel_size).to(device)
    net = torch.nn.DataParallel(net)

    criterion = nn.CrossEntropyLoss()
    # optimizer = torch.optim.SGD(net.parameters(), lr=0.1, momentum=0.9)
    optimizer = torch.optim.Adam(net.parameters(), lr=LEARNING_RATE)
    for epoch in range(100):
        running_loss = 0.0
        j = 0
        for i, data in enumerate(trainloader, 0):
            j += len(data[0])
            inputs, labels = data

            inputs, labels = inputs.to(device), labels.to(device)
            outputs = net.train()(inputs)
            loss = criterion(outputs, labels)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            running_loss += loss.data.item()

        print('epoch: {:04d} samples: {:6d} loss: {:.4f} train_acc: {:.2f}% test_acc: {:.2f}%'.format(
            epoch + 1, j, running_loss / len(x_train), accuracy(net, x_train.to(device), y_train) * 100,
            accuracy(net, x_test, y_test, filenames=n_test) * 100))

    torch.save(net, 'data/model.pickle')


if __name__ == '__main__':
    data_dir = "data/handwritten_letters/"
    main(41, 4, data_dir, True, 3, debug=True)
