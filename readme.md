## Create env

`conda.exe env create --file environment.yml`

## If you have a NVidia GPU

NVida GPU speeds things up considerably. To enable this, do:

`conda uninstall pytorch-cpu`

`conda install pytorch -c pytorch`


